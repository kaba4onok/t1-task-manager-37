package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.Task;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class TaskTestData {

    @NotNull
    public final static Task TASK1 = new Task("Task1", "Task1");

    @NotNull
    public final static Task TASK2 = new Task("Task2", "Task2");

    @NotNull
    public final static Task TASK3 = new Task("Task3", "Task3");

    @NotNull
    public final static Task NEW_TASK = new Task("NEW_TASK", "NEW_TASK");

    @NotNull
    public final static List<Task> USER1_TASKS = Arrays.asList(TASK1, TASK2);

    @NotNull
    public final static List<Task> USER2_TASKS = Collections.singletonList(TASK3);

}
