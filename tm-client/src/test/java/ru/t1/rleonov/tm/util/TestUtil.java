package ru.t1.rleonov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataBase64Request;
import ru.t1.rleonov.tm.dto.request.UserLoginRequest;
import ru.t1.rleonov.tm.dto.request.UserLogoutRequest;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.service.PropertyService;

public final class TestUtil {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    public static String login(@NotNull final User user) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(user.getLogin());
        request.setPassword(user.getPasswordHash());
        return authEndpoint.login(request).getToken();
    }

    public static void logout(@NotNull final String token) {
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        authEndpoint.logout(request);
    }

    public static void reloadData(@NotNull final String adminToken) {
        @NotNull final ServerLoadDataBase64Request request = new ServerLoadDataBase64Request(adminToken);
        domainEndpoint.loadDataBase64(request);
    }

}
