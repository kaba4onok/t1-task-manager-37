package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.User;
import static ru.t1.rleonov.tm.constant.UserTestData.USER1;
import static ru.t1.rleonov.tm.constant.UserTestData.USERS;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    /*@Test
    public void create() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final String login = USER1.getLogin();
        @Nullable final String passwordHash = USER1.getPasswordHash();
        @Nullable final User user = repository.create(login, passwordHash);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(passwordHash, user.getPasswordHash());
    }

    @Test
    public void createWithEmail() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final String login = USER1.getLogin();
        @Nullable final String passwordHash = USER1.getPasswordHash();
        @Nullable final String email = USER1.getEmail();
        @Nullable final User user = repository.create(login, passwordHash, email);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(passwordHash, user.getPasswordHash());
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void createWithRole() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final String login = USER1.getLogin();
        @Nullable final String passwordHash = USER1.getPasswordHash();
        @Nullable final Role role = USER1.getRole();
        @Nullable final User user = repository.create(login, passwordHash, role);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(passwordHash, user.getPasswordHash());
        Assert.assertEquals(role, user.getRole());
    }

    @Test
    public void finByLogin() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String login = USER1.getLogin();
        Assert.assertEquals(USER1, repository.findByLogin(login));
    }

    @Test
    public void finByEmail() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String email = USER1.getEmail();
        Assert.assertEquals(USER1, repository.findByEmail(email));
    }

    @Test
    public void isLoginExist() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String login = USER1.getLogin();
        Assert.assertTrue(repository.isLoginExist(login));
    }

    @Test
    public void isEmailExist() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String email = USER1.getEmail();
        Assert.assertTrue(repository.isEmailExist(email));
    }*/

}
