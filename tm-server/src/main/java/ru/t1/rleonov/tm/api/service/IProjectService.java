package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable
    Project create(
            @Nullable String userId,
            @Nullable String name);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status);

}
