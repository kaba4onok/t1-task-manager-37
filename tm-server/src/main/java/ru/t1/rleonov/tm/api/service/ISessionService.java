package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.model.Session;
import java.sql.Connection;

public interface ISessionService extends IUserOwnedService<Session> {

    @NotNull
    ISessionRepository getRepository(@NotNull Connection connection);

}
