package ru.t1.rleonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.Project;
import java.sql.ResultSet;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    @NotNull
    String getTableName();

    @NotNull
    Project fetch(@NotNull ResultSet row);

    @NotNull
    Project update(@NotNull Project project);

}
