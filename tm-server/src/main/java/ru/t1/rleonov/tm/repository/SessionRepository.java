package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Session;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return "tm.session";
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session fetch(@NotNull ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setName(row.getString("name"));
        session.setDescription(row.getString("description"));
        session.setStatus(Status.toStatus(row.getString("status")));
        session.setCreated(row.getTimestamp("created"));
        session.setRole(Role.toRole(row.getString("role")));
        session.setUserId(row.getString("user_id"));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, name, description, status, created, role, user_id) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?);",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getName());
            statement.setString(3, session.getDescription());
            statement.setString(4, session.getStatus().toString());
            statement.setTimestamp(5, new Timestamp(session.getCreated().getTime()));
            statement.setString(6, session.getRole().toString());
            statement.setString(7, session.getUserId());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull String userId, @NotNull Session session) {
        session.setUserId(userId);
        return add(session);
    }

}
