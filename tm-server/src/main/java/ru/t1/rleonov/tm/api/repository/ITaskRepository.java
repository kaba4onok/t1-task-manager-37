package ru.t1.rleonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.Task;
import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    @NotNull
    String getTableName();

    @NotNull
    Task fetch(@NotNull ResultSet row);

    @NotNull
    Task update(@NotNull Task task);

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId);

}
