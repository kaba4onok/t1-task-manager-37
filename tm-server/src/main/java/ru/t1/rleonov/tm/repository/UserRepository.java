package ru.t1.rleonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return "tm.user";
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password"));
        user.setEmail(row.getString("email"));
        user.setLastName(row.getString("last_name"));
        user.setFirstName(row.getString("first_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setRole(Role.toRole(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password, email, last_name, first_name, middle_name, role, locked) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET " +
                        "login = ?, " +
                        "password = ?, " +
                        "email = ?, " +
                        "last_name = ?, " +
                        "first_name = ?, " +
                        "middle_name = ?, " +
                        "role = ?, " +
                        "locked = ? " +
                        "WHERE id = ?;", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().toString());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @SneakyThrows
    public Boolean isLoginExist(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.next();
        }
    }

    @Override
    @SneakyThrows
    public Boolean isEmailExist(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1;", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.next();
        }
    }

}
