package ru.t1.rleonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ServerLoadDataBase64Request extends AbstractUserRequest {

    public ServerLoadDataBase64Request(@Nullable String token) {
        super(token);
    }

}
